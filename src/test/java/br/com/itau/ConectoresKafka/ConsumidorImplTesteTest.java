package br.com.itau.ConectoresKafka;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.MockConsumer;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.apache.kafka.common.TopicPartition;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.ConsumidorHandler;
import br.com.itau.kafka.consumidor.comtest.ConsumidorFactoryTeste;
import br.com.itau.kafka.consumidor.normal.ConsumidorHandlerImpl;

public class ConsumidorImplTesteTest {
	MockConsumer<String, JsonNode> consumer;
	public static int count=0;
	@Before
	public void setUp() {
	    consumer = new MockConsumer<String, JsonNode>(OffsetResetStrategy.EARLIEST);
	}
	
	@Test
	public void teste_consumidor_mockado() throws InterruptedException {
		ConsumidorFactoryTeste consumidorFactory = new ConsumidorFactoryTeste(consumer);
		Consumidor<Conta> consumidor = consumidorFactory.create(Conta.class, 100);
		String topic ="teste";
		TopicPartition topicPartition = new TopicPartition(topic, 0);
		consumer.assign(Collections.singletonList(topicPartition));
		HashMap<TopicPartition, Long> beginningOffsets = new HashMap<>();
		beginningOffsets.put(topicPartition, 0L);
		consumer.updateBeginningOffsets(beginningOffsets);
		Conta conta = new Conta("0933","03237","0","000001",ThreadLocalRandom.current().nextDouble(0, 4000));
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		JsonNode jsonNode = objectMapper.valueToTree(conta);
		consumer.addRecord(new ConsumerRecord<String,JsonNode>("teste", 0, 0L, "a", jsonNode));
		consumidor.receive(new ConsumidorHandler<Conta>() {
			
			@Override
			public void handle(Conta objeto) throws InterruptedException {
				if (objeto.getAgencia()=="0933")
				{
					System.out.println(objeto.toString());
					count++;
					consumidor.close();
				}
				
			}
		});
		Thread.sleep(1000);
		assertTrue(count>0);		
	}
	@Test(expected=NullPointerException.class)
	public void teste_consumidor_mockado_erro_pool_time() {
		ConsumidorFactoryTeste consumidorFactory = new ConsumidorFactoryTeste(consumer);
		long valor = (Long) null;
		Consumidor<Conta> consumidor = consumidorFactory.create(Conta.class, valor);
	}
	@Test
	public void teste_consumidor_mockado_nao_dropa() throws IOException {
		ConsumidorFactoryTeste consumidorFactory = new ConsumidorFactoryTeste(consumer);
		Consumidor<Conta> consumidor = consumidorFactory.create(Conta.class, 100);
		String topic ="teste";
		TopicPartition topicPartition = new TopicPartition(topic, 0);
		consumer.assign(Collections.singletonList(topicPartition));
		HashMap<TopicPartition, Long> beginningOffsets = new HashMap<>();
		beginningOffsets.put(topicPartition, 0L);
		consumer.updateBeginningOffsets(beginningOffsets);
		Conta conta = new Conta("0933","03237","0","000001",ThreadLocalRandom.current().nextDouble(0, 4000));
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		JsonNode jsonNode = objectMapper.valueToTree(conta);
		
		consumer.addRecord(new ConsumerRecord<String,JsonNode>("teste", 0, 0L, "a", jsonNode));
		consumidor.receive(new ConsumidorHandler<Conta>() {			
			@Override
			public void handle(Conta objeto) {
				consumidor.close();
				throw new ExcecaoTeste(objeto.toString());
			}
		});		
		
	}
	@Test
	public void teste_consumidor_handler_impl() throws InterruptedException
	{
		ConsumidorHandlerImpl<String> consumidorHandler = new ConsumidorHandlerImpl<>();
		consumidorHandler.handle("Ola");
	}
	@Test
	public void teste_consumidor_mockado2()
	{
		Properties props2 = new Properties();
		props2.put("bootstrap.servers", "localhost:9092");
		props2.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props2.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		props2.put("group.id", "grupo-1");
		props2.put("enable.auto.commit", "false");
		props2.put("session.timeout.ms", "15000");
		props2.put("auto.offset.reset", "earliest");
		props2.put("heartbeat.interval.ms", "5000");
		ConsumidorFactoryTeste consumidorFactory = new ConsumidorFactoryTeste(props2,"teste-dc3");
		Consumidor<Conta> consumidor = consumidorFactory.create(Conta.class,1000);
	}
}
class ExcecaoTeste extends RuntimeException
{
	public ExcecaoTeste(String destino) {
        super(destino.toString());
         
    }
}



