package br.com.itau.ConectoresKafka;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.produtor.ProdutorWithKey;
import br.com.itau.kafka.produtor.comtest.ProdutorFactoryTeste;
import junit.framework.TestCase;

public class ProdutorImplTesteTest extends TestCase {
	MockProducer<String, JsonNode> producer;
	@Before
	public void setUp()
	{
		producer = new MockProducer<String, JsonNode>(true,null,null);
	}
	@Test
	public void teste_produtor()
	{
		ProdutorFactoryTeste produtorfactory = new ProdutorFactoryTeste(producer);
		ProdutorWithKey<String> produtor = produtorfactory.create("teste");
		produtor.send("a", "1");
		List<ProducerRecord<String, JsonNode>> history = producer.history();
		String row = "a";
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		JsonNode jsonNode = objectMapper.valueToTree(row);
		List<ProducerRecord<String, JsonNode>> expected = new ArrayList<>();
		expected.add(new ProducerRecord<String, JsonNode>("teste", "1", jsonNode));
		produtor.close();
		 Assert.assertEquals(expected,history);
		
	}
	@Test
	public void teste_produtor_factory()
	{
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 200000);
		props.put("linger.ms", 100);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		props.put("compression.type", "lz4");
		ProdutorFactoryTeste produtorfactory = new ProdutorFactoryTeste(props);
		
	}
}
