package br.com.itau.testesHolder;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import br.com.itau.kafka.consumidor.ConsumidorHandler;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalRespostaHandlerImpl implements TransactionHandler<EntidadeHolder>{
	private int i=0;

	@Override
	public EntidadeHolder handle(EntidadeHolder objeto) throws InterruptedException {
			
		if(objeto.getEstado() == "Holder")
		{
			objeto.setEstado("Callback");
			return objeto;
		}
		else 
		{
			return null;
		}
	}
}
