package br.com.itau.testesHolder;

import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import br.com.itau.kafka.produtor.ProdutorWithKey;
import br.com.itau.kafka.produtor.comtest.ProdutorFactoryTeste;
import br.com.itau.kafka.transacional.Transaction;
import br.com.itau.kafka.transaction.TransacionalFactory;

@Controller
@RequestMapping("/proxy")
public class AppHolder {
	private final long _timeout = 60000;
    private final Map<UUID, DeferredResult<EntidadeHolder>> _map = new ConcurrentHashMap<>();

    private static final Logger _log = LoggerFactory.getLogger(AppHolder.class);
    
	@RequestMapping(value="wait", method = RequestMethod.POST)
    public @ResponseBody DeferredResult<EntidadeHolder> wait(@RequestBody EntidadeHolder operation) throws InterruptedException {
		
        UUID waitId = UUID.randomUUID();
        DeferredResult<EntidadeHolder> result = new DeferredResult<>(_timeout);
        result.onTimeout(() -> {
            _map.remove(waitId);
            _log.info("Limpou por timeout: " + waitId);
        });
        result.onCompletion(() -> {
            _map.remove(waitId);
            _log.info("Limpou: " + waitId);
        });
        _log.info("Add lista: " + waitId);
        _map.put(waitId, result);
        startTransacionalHolder(_map);
        EntidadeHolder entidadeHolder = new EntidadeHolder();
        entidadeHolder.setEstado("Callback");
        entidadeHolder.setId(waitId);
        entidadeHolder.setValor(100.00);
//        callDoSomething(entidadeHolder);
        return result;
    }

	private void startTransacionalHolder(Map<UUID, DeferredResult<EntidadeHolder>> _map2) throws InterruptedException {
		Properties propsProdutor = new Properties();
		propsProdutor.put("bootstrap.servers", "localhost:9092");
		propsProdutor.put("acks", "all");
		propsProdutor.put("transactional.id", "xpto234");
		propsProdutor.put("retries", 1);	
		propsProdutor.put("enable.idempotence", true);
		propsProdutor.put("transaction.timeout.ms", 5000);
		propsProdutor.put("batch.size", 200000);
		propsProdutor.put("linger.ms", 100);
		propsProdutor.put("buffer.memory", 33554432);
		propsProdutor.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		propsProdutor.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		propsProdutor.put("compression.type", "lz4");
	
		Properties propsConsumidor = new Properties();
		propsConsumidor.put("bootstrap.servers", "localhost:9092");
		propsConsumidor.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		propsConsumidor.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		propsConsumidor.put("group.id", "grupo-2");
		propsConsumidor.put("enable.auto.commit", "false");
		propsConsumidor.put("session.timeout.ms", "15000");
		propsConsumidor.put("auto.offset.reset", "earliest");
		propsConsumidor.put("heartbeat.interval.ms", "5000");
		propsConsumidor.put("isolation.level", "read_committed");
		
		TransacionalFactory transacionalFactory = new TransacionalFactory();
		Transaction<EntidadeHolder> transaction = transacionalFactory.create("testeHolder", "testeHolder", propsProdutor, propsConsumidor, EntidadeHolder.class, EntidadeHolder.class);
		transaction.start(new TransacionalHolderHandlerImpl(_map2), new TransacionalHolderHandlerImplAbort());
		
	}

	private void callDoSomething(EntidadeHolder entidadeHolder) {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 200000);
		props.put("linger.ms", 100);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		props.put("compression.type", "lz4");
		
		ProdutorFactoryTeste produtorfactoryteste= new ProdutorFactoryTeste(props);
		ProdutorWithKey<EntidadeHolder> produtorfactory = produtorfactoryteste.create("testeHolder");
		produtorfactory.send(entidadeHolder,"1");
//		produtorfactory.close();
		
	}

}
