package br.com.itau.testesHolder;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import br.com.itau.kafka.consumidor.ConsumidorHandler;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalRespostaHandlerImplAbort implements TransactionHandler<EntidadeHolder>{

	@Override
	public EntidadeHolder handle(EntidadeHolder objeto) throws InterruptedException {
		System.out.println(objeto.toString());
		return null;
		
	}
}
