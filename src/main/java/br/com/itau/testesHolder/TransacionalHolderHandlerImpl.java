package br.com.itau.testesHolder;

import java.util.Map;
import java.util.UUID;

import org.springframework.web.context.request.async.DeferredResult;

import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalHolderHandlerImpl implements TransactionHandler<EntidadeHolder>{
	private int i=0;
	private final Map<UUID, DeferredResult<EntidadeHolder>> _map2;
	public TransacionalHolderHandlerImpl(Map<UUID, DeferredResult<EntidadeHolder>> _map2) {
		this._map2=_map2;
	}

	@Override
	public EntidadeHolder handle(EntidadeHolder objeto) throws InterruptedException {
		System.out.println(objeto.toString());
		String estadoesperado = "Callback";
		String estado = objeto.getEstado();
		if(estado.equals(estadoesperado))
		{
			objeto.setEstado("Final");
			DeferredResult<EntidadeHolder> waiter = _map2.get(objeto.getId());
			if(waiter!=null)
			{
			waiter.setResult(objeto);
			}
			return objeto;
		}
		else 
		{
			return null;
		}
	}
}
