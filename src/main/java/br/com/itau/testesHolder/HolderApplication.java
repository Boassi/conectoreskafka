package br.com.itau.testesHolder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class HolderApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolderApplication.class, args);
	}
}
