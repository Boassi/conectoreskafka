package br.com.itau.testesHolder;

import java.util.Properties;

import br.com.itau.ConectoresKafka.Conta;
import br.com.itau.kafka.transacional.Transaction;
import br.com.itau.kafka.transaction.TransacionalFactory;
import br.com.itau.kafka.transaction.TransacionalHandlerImpl;
import br.com.itau.kafka.transaction.TransacionalHandlerImplAbort;

public class TransacionalHolder {

	public static void main(String[] args) throws InterruptedException {
		Properties propsProdutor = new Properties();
		propsProdutor.put("bootstrap.servers", "localhost:9092");
		propsProdutor.put("acks", "all");
		propsProdutor.put("transactional.id", "xpto");
		propsProdutor.put("retries", 1);	
		propsProdutor.put("enable.idempotence", true);
		propsProdutor.put("transaction.timeout.ms", 5000);
		propsProdutor.put("batch.size", 200000);
		propsProdutor.put("linger.ms", 100);
		propsProdutor.put("buffer.memory", 33554432);
		propsProdutor.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		propsProdutor.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		propsProdutor.put("compression.type", "lz4");
	
		Properties propsConsumidor = new Properties();
		propsConsumidor.put("bootstrap.servers", "localhost:9092");
		propsConsumidor.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		propsConsumidor.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		propsConsumidor.put("group.id", "grupo-1");
		propsConsumidor.put("enable.auto.commit", "false");
		propsConsumidor.put("session.timeout.ms", "15000");
		propsConsumidor.put("auto.offset.reset", "earliest");
		propsConsumidor.put("heartbeat.interval.ms", "5000");
		propsConsumidor.put("isolation.level", "read_committed");
		
		TransacionalFactory transacionalFactory = new TransacionalFactory();
		Transaction<EntidadeHolder> transaction = transacionalFactory.create("testeHolder", "testeHolder", propsProdutor, propsConsumidor, EntidadeHolder.class, EntidadeHolder.class);
		transaction.start(new TransacionalRespostaHandlerImpl(), new TransacionalRespostaHandlerImplAbort());
	}
}
