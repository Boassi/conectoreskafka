package br.com.itau.kafka.consumidor;

public interface Consumidor<T> {
	void receive(ConsumidorHandler<T> handler);
	void close();
}
