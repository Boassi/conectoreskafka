package br.com.itau.kafka.consumidor.normal;

import java.util.Properties;

import br.com.itau.kafka.consumidor.Consumidor;

public class ConsumidorFactory {
	public static <T> Consumidor<T> create(String topicName, Class<T> eventType,Properties props,long poolTime)
	{
		Consumidor<T> consumer = new ConsumidorImpl<T>(topicName,eventType,props,poolTime);
		return consumer;
	}
}
