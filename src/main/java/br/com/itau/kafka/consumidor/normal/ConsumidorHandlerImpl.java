package br.com.itau.kafka.consumidor.normal;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import br.com.itau.kafka.consumidor.ConsumidorHandler;

public class ConsumidorHandlerImpl<T> implements ConsumidorHandler<T>{
	private int i=0;
	@Override
	public void handle(T objeto){
		LocalDateTime ini=null,fim=null;
		i++;
		System.out.println(i);
		if(i==1)
		{
			ini = LocalDateTime.now();
		}
		if(i==100000)
		{
			fim = LocalDateTime.now();
			long seconds = ChronoUnit.MILLIS.between(ini, fim);
			System.out.println("Inicio = "+ini+" ---- Fim = "+fim+" ---- Diferença = "+seconds);
		}
	}
}
