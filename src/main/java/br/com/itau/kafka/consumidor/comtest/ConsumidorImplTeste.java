package br.com.itau.kafka.consumidor.comtest;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.ConsumidorHandler;

public class ConsumidorImplTeste<T> implements Consumidor<T> {
	private final Class<T> eventType;
	private final long poolTime;
	private final Consumer<String, JsonNode> consumer;
	private ConsumerRecords<String, JsonNode> consumerRecords;
	private JsonNode jsonNode;
	private boolean teste;

	public ConsumidorImplTeste(Class<T> eventType, Consumer<String, JsonNode> consumidor, long poolTime) {
		this.eventType = eventType;
		this.poolTime = poolTime;
		consumer = consumidor;	
		teste = true;
	}

	@Override
	public void receive(ConsumidorHandler<T> handler) {
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();

			while (teste) {
				consumerRecords = consumer.poll(poolTime);
				consumerRecords.forEach(record -> {
					jsonNode = record.value();
					try {
						T evento = objectMapper.treeToValue(jsonNode, eventType);
						handler.handle(evento);
					} 
					catch (Exception e)
					{
						e.printStackTrace();
					}
				});
				consumer.commitAsync();
			}
		

	}

	@Override
	public void close() {
			teste =false;
	}

}
