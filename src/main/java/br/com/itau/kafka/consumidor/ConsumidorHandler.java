package br.com.itau.kafka.consumidor;

public interface ConsumidorHandler<T> {
	void handle(T objeto) throws InterruptedException;
}
