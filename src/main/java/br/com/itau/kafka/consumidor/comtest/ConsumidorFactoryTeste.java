package br.com.itau.kafka.consumidor.comtest;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;

import com.fasterxml.jackson.databind.JsonNode;

import br.com.itau.kafka.consumidor.Consumidor;

public class ConsumidorFactoryTeste {
	private static Consumer<String, JsonNode> consumidor;
	
	public ConsumidorFactoryTeste(Properties props, String topicName) 
	{		
		consumidor = new KafkaConsumer<String, JsonNode>(props);
		consumidor.subscribe(Collections.singletonList(topicName));
	}
	public ConsumidorFactoryTeste(Consumer consumidor) 
	{
		this.consumidor = consumidor;
	}
	public static <T> Consumidor<T> create(Class<T> eventType,long poolTime)
	{
		Consumidor<T> consumer = new ConsumidorImplTeste<T>(eventType,consumidor,poolTime);
		return consumer;
	}
}
