package br.com.itau.kafka.consumidor.normal;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.ConsumidorHandler;

public class ConsumidorImpl<T> implements Consumidor<T> {
	private final String topicName;
	private final Class<T> eventType;
	private final Properties props;
	private final long poolTime;
	private final KafkaConsumer<String, JsonNode> consumer;
	private ConsumerRecords<String, JsonNode> consumerRecords;
	private JsonNode jsonNode;

	public ConsumidorImpl(String topicName, Class<T> eventType, Properties props, long poolTime) {
		this.topicName = topicName;
		this.eventType = eventType;
		this.props = props;
		this.poolTime = poolTime;
		consumer = new KafkaConsumer<String, JsonNode>(props);
		consumer.subscribe(Collections.singletonList(topicName));
	}

	@Override
	public void receive(ConsumidorHandler<T> handler) {
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		try {
			while (true) {
				consumerRecords = consumer.poll(poolTime);
				consumerRecords.forEach(record -> {
					jsonNode = record.value();
					try {
						T evento = objectMapper.treeToValue(jsonNode, eventType);
						handler.handle(evento);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				});
				consumer.commitAsync();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}
	@Override
	public void close() {
		consumer.close();
		
	}

}
