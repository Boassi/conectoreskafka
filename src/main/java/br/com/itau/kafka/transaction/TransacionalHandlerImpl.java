package br.com.itau.kafka.transaction;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import br.com.itau.kafka.consumidor.ConsumidorHandler;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalHandlerImpl<T> implements TransactionHandler<T>{
	private int i=0;

	LocalDateTime ini=null,fim=null;
	@Override
	public T handle(T objeto){
		i++;
		System.out.println(i);
		if(i==1)
		{
			ini = LocalDateTime.now();
		}

		if(i==100000)
		{
			fim = LocalDateTime.now();
			long seconds = ChronoUnit.MILLIS.between(ini, fim);
			System.out.println("Inicio = "+ini+" ---- Fim = "+fim+" ---- Diferença = "+seconds);
			return null;
		}
		return objeto;
	}
}
