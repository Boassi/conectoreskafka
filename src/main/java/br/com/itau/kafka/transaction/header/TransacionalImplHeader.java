package br.com.itau.kafka.transaction.header;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.header.Header;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.transacional.Transaction;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalImplHeader<T> implements Transaction<T> {
	private final Class<T> eventTypeProdutor,eventTypeConsumidor;
	private final String topicProdutor,topicConsumidor;
	private final Properties propsprodutor,propsconsumidor;
	private final KafkaProducer<String, JsonNode> producer;
	private final KafkaConsumer<String, byte[]> consumer;
	private ConsumerRecords<String, byte[]> consumerRecords;
	private JsonNode jsonNodeProdutor,jsonNodeConsumidor;
	private boolean estado;
	private ObjectMapper objectMapper;
	private final ExecutorService poolingThread;
	private final HashMap<String,List<String>> eventosAceitos;
	private int contador =0;
	public TransacionalImplHeader(String topicProdutor,String topicConsumidor,Properties propsProdutor,Properties propsConsumidor,Class<T> eventTypeConsumidor,Class<T> eventTypeProdutor, HashMap<String,List<String>> eventosAceitos)
	{
		this.eventTypeProdutor=eventTypeProdutor;
		this.eventTypeConsumidor=eventTypeConsumidor;
		this.topicProdutor=topicProdutor;
		this.topicConsumidor = topicConsumidor;
		this.propsprodutor=propsProdutor;
		this.propsconsumidor=propsConsumidor;
		producer = new KafkaProducer<String, JsonNode>(propsprodutor);
		consumer = new KafkaConsumer<String, byte[]>(propsconsumidor);
		consumer.subscribe(Collections.singletonList(topicConsumidor));
		estado = true;
		this.poolingThread = Executors.newSingleThreadExecutor();
		this.eventosAceitos=eventosAceitos;
	}
	@Override
	public void close() {
		estado = false;
		
	}
	@Override
	public void start(TransactionHandler<T> handler,TransactionHandler<T> handlerAbort) throws InterruptedException {
		objectMapper = new ObjectMapper().findAndRegisterModules();
		poolingThread.execute(()->executaConsumidor(handler,handlerAbort));
			
	}
	private void executaConsumidor(TransactionHandler<T> handler,TransactionHandler<T> handlerAbort)
	{
		producer.initTransactions();
		while (estado) {
			consumerRecords = consumer.poll(Long.MAX_VALUE);
			contador =0;
			try
			{
				HashMap<String,String> map = null;
				producer.beginTransaction();
				for (ConsumerRecord<String, byte[]> record : consumerRecords) {
					byte[] message = record.value();
					Iterator<Header> headers = record.headers().iterator();
					while (headers.hasNext()) {
						Header header = headers.next();
						map.put(header.key(), header.value().toString());						
					}
					List<String> tiposAceitos = eventosAceitos.get(map.get("fase"));
					if(tiposAceitos!=null)
					{
						if(tiposAceitos.contains(map.get("eventType")))
						{
							try {
				    			T evento = objectMapper.readValue(message, eventTypeConsumidor);
				    			evento = handler.handle(evento);
				    			if(evento != null)
								{
				    				ProducerRecord<String, JsonNode> recordsend = new ProducerRecord<String, JsonNode>(topicProdutor,jsonNodeProdutor);
									jsonNodeProdutor = objectMapper.valueToTree(evento);
									recordsend.headers().add("eventType","TEF_SOLICITADA".getBytes());
									recordsend.headers().add("fase",map.get("fase").getBytes());
									producer.send(recordsend);
								}
				    		}
							catch (Exception e)
							{
								e.printStackTrace();
								//handlerAbort.handle(null);
								producer.abortTransaction();
								
							}
						}
					}
					contador++;
					Map<TopicPartition,OffsetAndMetadata> offset = new HashMap<TopicPartition, OffsetAndMetadata>();
					TopicPartition topicPartition = new TopicPartition(topicConsumidor,record.partition());
					OffsetAndMetadata meta = new OffsetAndMetadata(1);
					offset.put(topicPartition, meta);
					producer.sendOffsetsToTransaction(offset, "grupo-9");
				}
				
				producer.commitTransaction();
			}
			catch(ProducerFencedException e) {
			  e.printStackTrace();
			  producer.close();
			} catch(KafkaException e) {
//			  handlerAbort.handle(null);
			  e.printStackTrace();
			  producer.abortTransaction();
			}
			
		
		}
	}
}
