package br.com.itau.kafka.transaction;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.transacional.Transaction;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalImpl<T> implements Transaction<T> {
	private final Class<T> eventTypeProdutor,eventTypeConsumidor;
	private final String topicProdutor,topicConsumidor;
	private final Properties propsprodutor,propsconsumidor;
	private final KafkaProducer<String, JsonNode> producer;
	private final KafkaConsumer<String, JsonNode> consumer;
	private ConsumerRecords<String, JsonNode> consumerRecords;
	private JsonNode jsonNodeProdutor,jsonNodeConsumidor;
	private boolean estado;
	private ObjectMapper objectMapper;
	private final ExecutorService poolingThread;
	public TransacionalImpl(String topicProdutor,String topicConsumidor,Properties propsProdutor,Properties propsConsumidor,Class<T> eventTypeConsumidor,Class<T> eventTypeProdutor)
	{
		this.eventTypeProdutor=eventTypeProdutor;
		this.eventTypeConsumidor=eventTypeConsumidor;
		this.topicProdutor=topicProdutor;
		this.topicConsumidor = topicConsumidor;
		this.propsprodutor=propsProdutor;
		this.propsconsumidor=propsConsumidor;
		producer = new KafkaProducer<String, JsonNode>(propsprodutor);
		consumer = new KafkaConsumer<String, JsonNode>(propsconsumidor);
		consumer.subscribe(Collections.singletonList(topicConsumidor));
		estado = true;
		this.poolingThread = Executors.newSingleThreadExecutor();
	}
	@Override
	public void close() {
		estado = false;
		
	}
	@Override
	public void start(TransactionHandler<T> handler,TransactionHandler<T> handlerAbort) throws InterruptedException {
		objectMapper = new ObjectMapper().findAndRegisterModules();
		poolingThread.execute(()->executaConsumidor(handler,handlerAbort));
			
	}
	private void executaConsumidor(TransactionHandler<T> handler,TransactionHandler<T> handlerAbort)
	{
		producer.initTransactions();
		while (estado) {
			consumerRecords = consumer.poll(Long.MAX_VALUE);
			try
			{
				
				producer.beginTransaction();
				consumerRecords.forEach(record -> {
						jsonNodeConsumidor = record.value();
						try {
							T evento = objectMapper.treeToValue(jsonNodeConsumidor, eventTypeConsumidor);
							evento = handler.handle(evento);
							
							if(evento != null)
							{
								jsonNodeProdutor = objectMapper.valueToTree(evento);
								producer.send(new ProducerRecord<String, JsonNode>(topicProdutor,jsonNodeProdutor));
							}
							else
							{
								producer.close();
							}
						} catch (Exception e)
						{
							//handlerAbort.handle(null);
							producer.abortTransaction();
							
						}
				
					});
//				Entender com Toscano como isso entra no fluxo
//				 producer.sendOffsetsToTransaction(currentOffsets(consumer), group);
				 producer.commitTransaction();
			}
			catch(ProducerFencedException e) {
			  e.printStackTrace();
			  producer.close();
			} catch(KafkaException e) {
//			  handlerAbort.handle(null);
			  e.printStackTrace();
			  producer.abortTransaction();
			}
			
		
		}
	}
}
