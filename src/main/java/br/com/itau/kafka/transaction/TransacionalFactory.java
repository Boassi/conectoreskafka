package br.com.itau.kafka.transaction;

import java.util.Properties;

import br.com.itau.kafka.produtor.Produtor;
import br.com.itau.kafka.transacional.Transaction;

public class TransacionalFactory {

	public static <T> Transaction<T> create(String topicProdutor,String topicConsumidor,Properties propsProdutor,Properties propsConsumidor,Class<T> eventTypeConsumidor,Class<T> eventTypeProdutor)
	{
		Transaction<T> transaction = new TransacionalImpl<T>(topicProdutor,topicConsumidor,propsProdutor,propsConsumidor,eventTypeConsumidor,eventTypeProdutor);		
		return transaction;
	}
}
