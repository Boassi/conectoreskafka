package br.com.itau.kafka.transaction.header;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import br.com.itau.kafka.transacional.Transaction;

public class TransacionalFactoryHeader {

	public static <T> Transaction<T> create(String topicProdutor,String topicConsumidor,Properties propsProdutor,Properties propsConsumidor,Class<T> eventTypeConsumidor,Class<T> eventTypeProdutor, HashMap<String,List<String>> eventosAceitos)
	{
		Transaction<T> transaction = new TransacionalImplHeader<T>(topicProdutor,topicConsumidor,propsProdutor,propsConsumidor,eventTypeConsumidor,eventTypeProdutor,eventosAceitos);		
		return transaction;
	}
}
