package br.com.itau.kafka.transaction.header;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import br.com.itau.kafka.consumidor.ConsumidorHandler;
import br.com.itau.kafka.transacional.TransactionHandler;

public class TransacionalHandlerImplAbortHeader<T> implements TransactionHandler<T>{
	private int i=0;
	@Override
	public T handle(T objeto){
		System.out.println("Abort");
		System.out.println(objeto.toString());
		return objeto;
	}
}
