package br.com.itau.kafka.transacional;

public interface Transaction<T> {
	void start(TransactionHandler<T> handler,TransactionHandler<T> handlerAbort) throws InterruptedException;
	void close();
}
