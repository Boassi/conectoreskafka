package br.com.itau.kafka.transacional;

public interface TransactionHandler<T> {
	T handle(T objeto) throws InterruptedException;
}
