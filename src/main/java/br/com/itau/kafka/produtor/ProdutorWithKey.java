package br.com.itau.kafka.produtor;

public interface ProdutorWithKey<T> {
	void send(T mensagem, String key);
	void close();
}
