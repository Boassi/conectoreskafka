package br.com.itau.kafka.produtor;

public interface Produtor<T> {
	void send(T mensagem);
	void close();
}
