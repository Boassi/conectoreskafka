package br.com.itau.kafka.produtor.comtest;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import com.fasterxml.jackson.databind.JsonNode;

import br.com.itau.kafka.produtor.ProdutorWithKey;

public class ProdutorFactoryTeste {
	
	private static Producer<String, JsonNode> producer;
	public ProdutorFactoryTeste(Properties props)
	{
		producer = new KafkaProducer<String, JsonNode>(props);
	}
	public ProdutorFactoryTeste(Producer producer)
	{
		this.producer = producer;
	}	
	public static <T> ProdutorWithKey<T> create(String topicname)
	{		
		ProdutorWithKey<T> produtor = new ProdutorImplTeste<T>(topicname,producer);		
		return produtor;
	}
}
