package br.com.itau.kafka.produtor.normal;

import java.util.Properties;

import br.com.itau.kafka.produtor.Produtor;

public class ProdutorFactory {

	public static <T> Produtor<T> create(String topicname,Properties props)
	{
		Produtor<T> produtor = new ProdutorImpl<T>(topicname,props);		
		return produtor;
	}
}
