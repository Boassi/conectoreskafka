package br.com.itau.kafka.produtor.comtest;

import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.produtor.ProdutorWithKey;

public class ProdutorImplTeste<T> implements ProdutorWithKey<T> {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(ProdutorImplTeste.class);
	private final String topicname;
	private final Producer<String, JsonNode> producer;
	
	public ProdutorImplTeste(String topicname,Producer producer)
	{
		this.topicname=topicname;
		this.producer = producer;
	}
	public void send(T objeto,String key) {
		
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		JsonNode jsonNode = objectMapper.valueToTree(objeto);
		producer.send(new ProducerRecord<String, JsonNode>(topicname,key,jsonNode));
	
	}
	@Override
	public void close() {
		producer.close();
		
	}

}
