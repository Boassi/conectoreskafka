package br.com.itau.kafka.produtor.withkey;

import java.util.Properties;

import br.com.itau.kafka.produtor.ProdutorWithKey;

public class ProdutorFactoryWithKey {

	public static <T> ProdutorWithKey<T> create(String topicname,Properties props)
	{
		ProdutorWithKey<T> produtor = new ProdutorImplWithKey<T>(topicname,props);		
		return produtor;
	}
}
