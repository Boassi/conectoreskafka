package br.com.itau.kafka.produtor.normal;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.produtor.Produtor;

public class ProdutorImpl<T> implements Produtor<T> {
	private final String topicname;
	private final Properties props;
	private final KafkaProducer producer;
	
	public ProdutorImpl(String topicname,Properties props)
	{
		this.topicname=topicname;
		this.props=props;
		producer = new KafkaProducer<String, JsonNode>(props);
	}
	public void send(T objeto) {
		
		ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
		JsonNode jsonNode = objectMapper.valueToTree(objeto);
		producer.send(new ProducerRecord<String, JsonNode>(topicname,jsonNode));
	
	}
	@Override
	public void close() {
		producer.close();
		
	}

}
