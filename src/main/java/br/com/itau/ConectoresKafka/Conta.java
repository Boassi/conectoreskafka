package br.com.itau.ConectoresKafka;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public class Conta {
		private String agencia;
		private String conta;
		private String dac;
		private String titularidade;
		private double valor;
		public Conta()
		{
			
		}
		public Conta(String agencia, String conta, String dac, String titularidade, double valor) {
			super();
			this.agencia = agencia;
			this.conta = conta;
			this.dac = dac;
			this.titularidade = titularidade;
			this.valor = valor;
		}
		public String getAgencia() {
			return agencia;
		}
		public void setAgencia(String agencia) {
			this.agencia = agencia;
		}
		public String getConta() {
			return conta;
		}
		public void setConta(String conta) {
			this.conta = conta;
		}
		public String getDac() {
			return dac;
		}
		public void setDac(String dac) {
			this.dac = dac;
		}
		public String getTitularidade() {
			return titularidade;
		}
		public void setTitularidade(String titularidade) {
			this.titularidade = titularidade;
		}
		
		public double getValor() {
			return valor;
		}
		public void setValor(double valor) {
			this.valor = valor;
		}
		@Override
		public String toString() {
			return "Conta [agencia=" + agencia + ", conta=" + conta + ", dac=" + dac + ", titularidade=" + titularidade
					+ ", valor=" + valor + "]";
		}

		
		
}
