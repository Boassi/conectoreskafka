package br.com.itau.ConectoresKafka;

import java.util.Properties;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.comtest.ConsumidorFactoryTeste;
import br.com.itau.kafka.consumidor.normal.ConsumidorHandlerImpl;

/**
 * Hello world!
 *
 */
public class AppConsumidorTeste {
	public static void main(String[] args) {
		
		Properties props2 = new Properties();
		props2.put("bootstrap.servers", "localhost:9092");
		props2.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props2.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		props2.put("group.id", "grupo-3");
		props2.put("enable.auto.commit", "false");
		props2.put("session.timeout.ms", "15000");
		props2.put("auto.offset.reset", "earliest");
		props2.put("heartbeat.interval.ms", "5000");
		ConsumidorFactoryTeste consumidorFactory = new ConsumidorFactoryTeste(props2,"teste");
		Consumidor<Conta> consumidor = consumidorFactory.create(Conta.class,Long.MAX_VALUE);
		consumidor.receive(new ConsumidorHandlerImpl<Conta>());		

	}
}
