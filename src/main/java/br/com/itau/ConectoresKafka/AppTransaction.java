package br.com.itau.ConectoresKafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.normal.ConsumidorFactory;
import br.com.itau.kafka.consumidor.normal.ConsumidorHandlerImpl;
import br.com.itau.kafka.transacional.Transaction;
import br.com.itau.kafka.transaction.TransacionalFactory;
import br.com.itau.kafka.transaction.TransacionalHandlerImpl;
import br.com.itau.kafka.transaction.TransacionalHandlerImplAbort;

public class AppTransaction {
		public static void main(String[] args) throws InterruptedException {
			
	
			Properties propsProdutor = new Properties();
			propsProdutor.put("bootstrap.servers", "localhost:9092");
			propsProdutor.put("acks", "all");
			propsProdutor.put("transactional.id", "xpto");
			propsProdutor.put("retries", 1);	
			propsProdutor.put("enable.idempotence", true);
			propsProdutor.put("transaction.timeout.ms", 5000);
			propsProdutor.put("batch.size", 200000);
			propsProdutor.put("linger.ms", 1000);
			propsProdutor.put("buffer.memory", 33554432);
			propsProdutor.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			propsProdutor.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
			propsProdutor.put("compression.type", "lz4");
		
			Properties propsConsumidor = new Properties();
			propsConsumidor.put("bootstrap.servers", "localhost:9092");
			propsConsumidor.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			propsConsumidor.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
			propsConsumidor.put("group.id", "grupo-6");
			propsConsumidor.put("enable.auto.commit", "false");
			propsConsumidor.put("session.timeout.ms", "15000");
			propsConsumidor.put("auto.offset.reset", "earliest");
			propsConsumidor.put("heartbeat.interval.ms", "5000");
			propsConsumidor.put("isolation.level", "read_committed");
			propsConsumidor.put("max.pool.records", "50");
			
			TransacionalFactory transacionalFactory = new TransacionalFactory();
			Transaction<Conta> transaction = transacionalFactory.create("teste2", "teste", propsProdutor, propsConsumidor, Conta.class, Conta.class);
			transaction.start(new TransacionalHandlerImpl<Conta>(), new TransacionalHandlerImplAbort<Conta>());
		}
}
