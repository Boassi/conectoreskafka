package br.com.itau.ConectoresKafka;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;

import br.com.itau.kafka.produtor.ProdutorWithKey;
import br.com.itau.kafka.produtor.withkey.ProdutorFactoryWithKey;

/**
 * Hello world!
 *
 */
public class AppProdutorWithKey {
	public static void main(String[] args) {
		HashMap<Integer, Conta> payload = new HashMap<Integer, Conta>();
		payload.put(0, new Conta("0933","03237","0","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(1, new Conta("0933","12345","1","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(2, new Conta("0933","23456","2","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(3, new Conta("0933","34567","3","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(4, new Conta("0933","45678","4","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(5, new Conta("0933","56789","5","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(6, new Conta("0933","67890","6","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(7, new Conta("0933","78901","7","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(8, new Conta("0933","89012","8","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));
		payload.put(9, new Conta("0933","90123","9","000001",ThreadLocalRandom.current().nextDouble(0, 4000)));

		
		String key="";
		Conta conta;
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 200000);
		props.put("linger.ms", 100);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		props.put("compression.type", "lz4");
		
		ProdutorWithKey<Conta> produtorfactory = ProdutorFactoryWithKey.create("teste-dc", props);
		LocalDateTime ini = LocalDateTime.now();
		for (int i = 0; i < 100; i++) {
			int j =i%10;
			produtorfactory.send(payload.get(j),payload.get(j).getAgencia().toString()+payload.get(j).getConta().toString()+payload.get(j).getDac().toString());
		}
		LocalDateTime fim = LocalDateTime.now();
		long seconds = ChronoUnit.MILLIS.between(ini, fim);
		System.out.println("Inicio = "+ini+" ---- Fim = "+fim+" ---- Diferença = "+seconds);
		produtorfactory.close();

	}
}
