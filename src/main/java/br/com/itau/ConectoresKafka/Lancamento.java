package br.com.itau.ConectoresKafka;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public class Lancamento {
		private UUID idLancamento;
		private double valor;
		private LocalDateTime data;
		public LocalDateTime getData() {
			return data;
		}
		public void setData(LocalDateTime data) {
			this.data = data;
		}
		public UUID getIdLancamento() {
			return idLancamento;
		}
		public void setIdLancamento(UUID idLancamento) {
			this.idLancamento = idLancamento;
		}
		public double getValor() {
			return valor;
		}
		public void setValor(double valor) {
			this.valor = valor;
		}
		@Override
		public String toString() {
			return "Lancamento [idLancamento=" + idLancamento + ", valor=" + valor + ", data=" + data + "]";
		}
	
		
}
