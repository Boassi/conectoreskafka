package br.com.itau.ConectoresKafka;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.normal.ConsumidorFactory;
import br.com.itau.kafka.consumidor.normal.ConsumidorHandlerImpl;
import br.com.itau.kafka.produtor.Produtor;
import br.com.itau.kafka.produtor.normal.ProdutorFactory;

/**
 * Hello world!
 *
 */
public class AppProdutor {
	public static void main(String[] args) {
		
		Lancamento lancamento;
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 200000);
		props.put("linger.ms", 100);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
		props.put("compression.type", "lz4");
		
		Produtor<Lancamento> produtorfactory = ProdutorFactory.create("testetrans", props);
		LocalDateTime ini = LocalDateTime.now();
		for (int i = 0; i < 1; i++) {
			lancamento = new Lancamento();
			lancamento.setIdLancamento(UUID.randomUUID());
			lancamento.setData(LocalDateTime.now());
			double random = ThreadLocalRandom.current().nextDouble(0, 4000);
			lancamento.setValor(random);
			produtorfactory.send(lancamento);
		}
		LocalDateTime fim = LocalDateTime.now();
		long seconds = ChronoUnit.MILLIS.between(ini, fim);
		System.out.println("Inicio = "+ini+" ---- Fim = "+fim+" ---- Diferença = "+seconds);
		produtorfactory.close();

	}
}
