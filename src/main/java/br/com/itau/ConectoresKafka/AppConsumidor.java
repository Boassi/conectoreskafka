package br.com.itau.ConectoresKafka;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import br.com.itau.kafka.consumidor.Consumidor;
import br.com.itau.kafka.consumidor.normal.ConsumidorFactory;
import br.com.itau.kafka.consumidor.normal.ConsumidorHandlerImpl;
import br.com.itau.kafka.produtor.Produtor;
import br.com.itau.kafka.produtor.normal.ProdutorFactory;

/**
 * Hello world!
 *
 */
public class AppConsumidor {
	public static void main(String[] args) {
		
		Properties props2 = new Properties();
		props2.put("bootstrap.servers", "localhost:9092");
		props2.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props2.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
		props2.put("group.id", "grupo-1");
		props2.put("enable.auto.commit", "false");
		props2.put("session.timeout.ms", "15000");
		props2.put("auto.offset.reset", "earliest");
		props2.put("heartbeat.interval.ms", "5000");
		
		Consumidor<Lancamento> consumidor = ConsumidorFactory.create("teste-dc", Lancamento.class, props2,1000);
		consumidor.receive(new ConsumidorHandlerImpl<Lancamento>());		

	}
}
